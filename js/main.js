const btn = document.querySelector('.btn');

const body = document.body;
// header
const headerTitle = document.querySelector('.title-link');
const headerIcons = document.querySelectorAll('.page-header_icon');
const headerLinks = document.querySelectorAll('.page-header_text-link');
// main
const formTitle = document.querySelector('.form__title');
const formLinks = document.querySelectorAll('.newLinkStyle');
const formBtns = document.querySelectorAll('.newBtnStyle');
const formCheckbox = document.querySelector('.checkbox');

function setItems(key, elem, ...cls) {
    cls.forEach((el) => {
        localStorage.setItem(key, elem.classList.contains(el) ? 'on' : 'off');
    });
}

function getAndAdd(key, elem, ...cls) {
    if (localStorage.getItem(key) === 'on') {
        cls.forEach((el) => {
            elem.classList.add(el);
        });
    }
}

function setItemsForEach(key, elem, ...cls) {
    elem.forEach((el)=>{
        setItems(key, el, ...cls);
    });
}

function getAndAddForEach(key, elem, ...cls) {
    elem.forEach((el)=>{
        getAndAdd(key, el, ...cls);
    });
}

function toggler(elem, ...cls) {
    cls.forEach((el) => {
        elem.classList.toggle(el);
    });
};

function togglerForEach(elem, ...cls) {
    elem.forEach(i => {
        cls.forEach((el) => {
            toggler(i, el);
        });
    });
};
//=====================================================================================
window.addEventListener('unload', () => {
    setItems('bgImage', body, 'new-background-img');
    // header
    setItems('colorForTit', headerTitle, 'new-color__hov');
    setItemsForEach('colorForLnk', headerLinks, 'new-color__hov');
    setItemsForEach('fill', headerIcons, 'new-fill');
    // main
    setItems('bgColorForTit', formTitle, 'new-bgcolor_bef');
    setItemsForEach('bdColor', formLinks, 'new-color', 'new-bd-col');
    setItemsForEach('bgColorForBtns', formBtns, 'new-bgcolor');
    setItems('checkbox', formCheckbox, 'new-checkbox');
});

getAndAdd('bgImage', body, 'new-background-img');
// header
getAndAdd('colorForTit', headerTitle, 'new-color__hov');
getAndAddForEach('colorForLnk', headerLinks, 'new-color__hov');
getAndAddForEach('fill', headerIcons, 'new-fill');
// main
getAndAdd('bgColorForTit', formTitle, 'new-bgcolor_bef');
getAndAddForEach('bdColor', formLinks, 'new-color', 'new-bd-col');
getAndAddForEach('bgColorForBtns', formBtns, 'new-bgcolor');
getAndAdd('checkbox', formCheckbox, 'new-checkbox');


btn.addEventListener('click', (evt) => {
    evt.preventDefault();
    toggler(body, 'new-background-img');
    // header
    toggler(headerTitle, 'new-color__hov');
    togglerForEach(headerIcons, 'new-fill');
    togglerForEach(headerLinks, 'new-color__hov');

    // main
    toggler(formTitle, 'new-bgcolor_bef');
    togglerForEach(formLinks, 'new-color', 'new-bd-col');
    togglerForEach(formBtns, 'new-bgcolor');
    toggler(formCheckbox, 'new-checkbox');
});
